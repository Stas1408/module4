﻿using System;
using System.ComponentModel.Design.Serialization;
using System.Threading.Tasks;

namespace M4
{
    public class Module4
    {
        static void Main(string[] args)
        {
        }


        public int Task_1_A(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException("array", "Entered array is empty");
            }
            int max = array[0];
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] > max)
                {
                    max = array[i];
                }
            }
            return max;

        }

        public int Task_1_B(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException("array", "Entered array is empty");
            }
            int min = array[0];
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] < min)
                {
                    min = array[i];
                }
            }
            return min;

        }

        public int Task_1_C(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException("array", "Entered array is empty");
            }
            int sum = 0;
            for (int i = 0; i < array.Length; i++)
            {
                sum += array[i];
            }
            return sum;

        }

        public int Task_1_D(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException("array", "Entered array is empty");
            }
            int max = array[0];
            int min = array[0];
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] > max)
                {
                    max = array[i];
                }
                if (array[i] < min)
                {
                    min = array[i];
                }
            }
            return max - min;

        }

        public void Task_1_E(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException("array", "Entered array is empty");
            }
            int max = array[0];
            int min = array[0];
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] > max)
                {
                    max = array[i];
                }
                if (array[i] < min)
                {
                    min = array[i];
                }
            }
            for (int i = 0; i < array.Length; i++)
            {
                if (i % 2 == 0)
                {
                    array[i] += max;
                }
                else
                {
                    array[i] -= min;
                }
            }


        }

        public int Task_2(int a, int b, int c)
        {
            return a + b + c;
        }

        public int Task_2(int a, int b)
        {
            return a + b;
        }

        public double Task_2(double a, double b, double c)
        {
            return a + b + c;
        }

        public string Task_2(string a, string b)
        {
            return a + b;
        }

        public int[] Task_2(int[] a, int[] b)
        {
            int min = (a.Length > b.Length) ? (b.Length) : (a.Length);
            for (int i = 0; i < min; i++)
            {
                if (min == a.Length)
                {
                    b[i] += a[i];
                }
                else
                {
                    a[i] += b[i];
                }
            }
            if (min == a.Length)
            {
                return b;
            }
            else
            {
                return a;
            }

        }

        public void Task_3_A(ref int a, ref int b, ref int c)
        {
            a += 10;
            b += 10;
            c += 10;
        }

        public void Task_3_B(double radius, out double length, out double square)
        {
            if (radius < 0)
            {
                throw new ArgumentException("Value does not fall within the expected range");
            }
            length = 2 * radius * Math.PI;
            square = Math.PI * radius * radius;

        }

        public void Task_3_C(int[] array, out int maxItem, out int minItem, out int sumOfItems)
        {
            maxItem = array[0];
            sumOfItems = 0;
            for (int i = 0; i < array.Length; i++)
            {
                sumOfItems += array[i];
                if (array[i] > maxItem)
                {
                    maxItem = array[i];
                }
            }
            minItem = array[0];
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] < minItem)
                {
                    minItem = array[i];
                }
            }


        }

        public (int, int, int) Task_4_A((int, int, int) numbers)
        {
            numbers.Item1 += 10;
            numbers.Item2 += 10;
            numbers.Item3 += 10;
            return numbers;

        }

        public (double, double) Task_4_B(double radius)
        {
            if (radius < 0)
            {
                throw new ArgumentException( "Value does not fall within the expected range","radius");
            }
            return (2 * radius * Math.PI, Math.PI * radius * radius);
        }

        public (int, int, int) Task_4_C(int[] array)
        {
            int maxItem = array[0];
            int sumOfItems = 0;
            for (int i = 0; i < array.Length; i++)
            {
                sumOfItems += array[i];
                if (array[i] > maxItem)
                {
                    maxItem = array[i];
                }
            }
            int minItem = array[0];
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] < minItem)
                {
                    minItem = array[i];
                }
            }
            return (minItem, maxItem, sumOfItems);
        }

        public void Task_5(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException("Value does not fall within the expected range");
            }
            for (int i = 0; i < array.Length; i++)
            {
                array[i] += 5;
            }
        }

        public void Task_6(int[] array, SortDirection direction)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException("Value does not fall within the expected range");
            }
            if (direction == SortDirection.Ascending)
            {
                Array.Sort(array);
            }
            else
            {
                Array.Sort(array);
                Array.Reverse(array);
            }

        }

        public double Task_7(Func<double, double> func, double x1, double x2, double e, double result = 0)
        {
            throw new NotImplementedException();
        }
    }
}
